# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :multipartform,
  ecto_repos: [Multipartform.Repo]

# Configures the endpoint
config :multipartform, MultipartformWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "C+UQgm7eeGxbACYua6z3ffb3CrG06/K/TstiIWe+wg3UDfbgGJFxjn/u4bVa6MXK",
  render_errors: [view: MultipartformWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Multipartform.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Drab template engine
config :phoenix, :template_engines, drab: Drab.Live.Engine

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
