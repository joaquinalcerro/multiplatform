defmodule Multipartform.Documents.Detail do
  use Ecto.Schema
  import Ecto.Changeset
  alias Multipartform.Documents.Detail

  schema "details" do
    field(:code, :integer)
    field(:desc, :string)
    field(:tax, :boolean, default: false)
    field(:up, :decimal)
    field(:delete, :boolean, virtual: true)
    belongs_to(:invoice, Multipartform.Documents.Invoice)

    timestamps()
  end

  @doc false
  def changeset(%Detail{} = detail, attrs) do
    detail
    |> cast(attrs, [:tax, :code, :desc, :up])
    |> validate_required([:tax, :code, :desc, :up])
  end

  @doc false
  def update_changeset(%Detail{invoice_id: nil}, attrs, invoice) do
    Ecto.build_assoc(invoice, :details)
    |> cast(attrs, [:tax, :code, :desc, :up, :invoice_id])
    |> validate_required([:tax, :code, :desc, :up, :invoice_id])
  end

  @doc false
  def update_changeset(%Detail{} = detail, attrs, _invoice) do
    detail
    |> cast(attrs, [:tax, :code, :desc, :up, :invoice_id, :delete])
    |> validate_required([:tax, :code, :desc, :up, :invoice_id])
    |> mark_for_delete()
  end

  @doc false
  defp mark_for_delete(changeset) do
    if get_change(changeset, :delete) do
      %{changeset | action: :delete}
    else
      changeset
    end
  end
end
