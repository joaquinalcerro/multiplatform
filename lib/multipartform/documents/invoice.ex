defmodule Multipartform.Documents.Invoice do
  use Ecto.Schema
  import Ecto.Changeset
  alias Multipartform.Documents.Invoice

  require IEx

  schema "invoices" do
    field(:provider, :string)
    has_many(:details, Multipartform.Documents.Detail)

    timestamps()
  end

  @doc false
  def changeset(%Invoice{} = invoice, attrs) do
    invoice
    |> cast(attrs, [:provider])
    |> validate_required([:provider])
    |> cast_assoc(:details)
  end

  @doc false
  def update_changeset(%Invoice{} = invoice, attrs) do
    invoice
    |> cast(attrs, [:provider])
    |> validate_required([:provider])
    |> cast_assoc(
      :details,
      with: &Multipartform.Documents.Detail.update_changeset(&1, &2, invoice)
    )
  end
end
