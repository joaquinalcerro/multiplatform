defmodule Multipartform.Documents do
  require IEx

  @moduledoc """
  The Documents context.
  """

  import Ecto.Query, warn: false
  alias Multipartform.Repo
  alias Ecto.Multi

  alias Multipartform.Documents.Invoice

  @doc """
  Returns the list of invoices.

  ## Examples

      iex> list_invoices()
      [%Invoice{}, ...]

  """
  def list_invoices do
    Repo.all(Invoice)
  end

  @doc """
  Gets a single invoice.

  Raises `Ecto.NoResultsError` if the Invoice does not exist.

  ## Examples

      iex> get_invoice!(123)
      %Invoice{}

      iex> get_invoice!(456)
      ** (Ecto.NoResultsError)

  """
  def get_invoice!(id), do: Repo.get!(Invoice, id) |> Repo.preload(:details)

  @doc """
  Creates a invoice.

  ## Examples

      iex> create_invoice(%{field: value})
      {:ok, %Invoice{}}

      iex> create_invoice(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  def create_invoice(attrs \\ %{}) do
      %Invoice{}
      |> Invoice.changeset(attrs)
      |> Repo.insert()
  end

  @doc """
  Updates a invoice.

  ## Examples

      iex> update_invoice(invoice, %{field: new_value})
      {:ok, %Invoice{}}

      iex> update_invoice(invoice, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_invoice(%Invoice{} = invoice, attrs) do
    invoice
    |> Invoice.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Invoice.

  ## Examples

      iex> delete_invoice(invoice)
      {:ok, %Invoice{}}

      iex> delete_invoice(invoice)
      {:error, %Ecto.Changeset{}}

  """
  def delete_invoice(%Invoice{} = invoice) do
    Repo.delete(invoice)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking invoice changes.

  ## Examples

      iex> change_invoice(invoice)
      %Ecto.Changeset{source: %Invoice{}}

  """
  def change_invoice(%Invoice{} = invoice) do
    Invoice.changeset(invoice, %{})
  end

  alias Multipartform.Documents.Detail

  @doc """
  Returns the list of details.

  ## Examples

      iex> list_details()
      [%Detail{}, ...]

  """
  def list_details do
    Repo.all(Detail)
  end

  @doc """
  Gets a single detail.

  Raises `Ecto.NoResultsError` if the Detail does not exist.

  ## Examples

      iex> get_detail!(123)
      %Detail{}

      iex> get_detail!(456)
      ** (Ecto.NoResultsError)

  """
  def get_detail!(id), do: Repo.get!(Detail, id)

  @doc """
  Creates a detail.

  ## Examples

      iex> create_detail(%{field: value})
      {:ok, %Detail{}}

      iex> create_detail(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_detail(attrs \\ %{}) do
    %Detail{}
    |> Detail.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a detail.

  ## Examples

      iex> update_detail(detail, %{field: new_value})
      {:ok, %Detail{}}

      iex> update_detail(detail, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_detail(%Detail{} = detail, attrs) do
    detail
    |> Detail.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Detail.

  ## Examples

      iex> delete_detail(detail)
      {:ok, %Detail{}}

      iex> delete_detail(detail)
      {:error, %Ecto.Changeset{}}

  """
  def delete_detail(%Detail{} = detail) do
    Repo.delete(detail)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking detail changes.

  ## Examples

      iex> change_detail(detail)
      %Ecto.Changeset{source: %Detail{}}

  """
  def change_detail(%Detail{} = detail) do
    Detail.changeset(detail, %{})
  end
end
