defmodule MultipartformWeb.Router do
  use MultipartformWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", MultipartformWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)

    resources("/invoices", InvoiceController)
    resources("/detail", DetailController)
  end

  # Other scopes may use custom stacks.
  # scope "/api", MultipartformWeb do
  #   pipe_through :api
  # end
end
