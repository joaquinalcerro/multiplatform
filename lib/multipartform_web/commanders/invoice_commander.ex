defmodule MultipartformWeb.InvoiceCommander do
  use Drab.Commander
  # Place your event handlers here
  #
  # def button_clicked(socket, sender) do
  #   set_prop socket, "#output_div", innerHTML: "Clicked the button!"
  # end
  #
  def add_item_invoice_list(socket, _sender) do
    insert_html(
      socket,
      "#detail",
      :beforeend,
      add_detail()
    )
  end

  def add_detail() do
    item_no = Enum.random(999..5999)

    """
      <tr>
        <td>
          <input 
            name="invoice[details][#{item_no}][tax]" 
            type="hidden" value="false">
          <input 
            class="checkbox" 
            id="invoice_details_#{item_no}_tax" 
            name="invoice[details][#{item_no}][tax]" 
            type="checkbox" 
            value="true">
        </td>
        <td>
          <input 
            class="form-control" 
            id="invoice_details_#{item_no}_code" 
            name="invoice[details][#{item_no}][code]" 
            type="number">
        </td>
        <td>
          <input 
            class="form-control" 
            id="invoice_details_#{item_no}_desc" 
            name="invoice[details][#{item_no}][desc]" 
            type="text">
        </td>
        <td>
          <input 
            class="form-control" 
            id="invoice_details_#{item_no}_up" 
            name="invoice[details][#{item_no}][up]" 
            step="any" 
            type="number">
        </td>
        <td>
          <input 
            class="checkbox" 
            id="invoice_details_#{item_no}_delete" 
            name="invoice[details][#{item_no}][delete]" 
            type="checkbox" 
            value="true">
        </td>
      </tr>
    """
  end

  # Place you callbacks here
  #
  # onload :page_loaded 
  #
  # def page_loaded(socket) do
  #   set_prop socket, "div.jumbotron h2", innerText: "This page has been drabbed"
  # end
end
