defmodule MultipartformWeb.DetailController do
  use MultipartformWeb, :controller

  alias Multipartform.Documents
  alias Multipartform.Documents.Detail

  def index(conn, _params) do
    details = Documents.list_details()
    render(conn, "index.html", details: details)
  end

  def new(conn, _params) do
    changeset = Documents.change_detail(%Detail{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"detail" => detail_params}) do
    case Documents.create_detail(detail_params) do
      {:ok, detail} ->
        conn
        |> put_flash(:info, "Detail created successfully.")
        |> redirect(to: detail_path(conn, :show, detail))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    detail = Documents.get_detail!(id)
    render(conn, "show.html", detail: detail)
  end

  def edit(conn, %{"id" => id}) do
    detail = Documents.get_detail!(id)
    changeset = Documents.change_detail(detail)
    render(conn, "edit.html", detail: detail, changeset: changeset)
  end

  def update(conn, %{"id" => id, "detail" => detail_params}) do
    detail = Documents.get_detail!(id)

    case Documents.update_detail(detail, detail_params) do
      {:ok, detail} ->
        conn
        |> put_flash(:info, "Detail updated successfully.")
        |> redirect(to: detail_path(conn, :show, detail))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", detail: detail, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    detail = Documents.get_detail!(id)
    {:ok, _detail} = Documents.delete_detail(detail)

    conn
    |> put_flash(:info, "Detail deleted successfully.")
    |> redirect(to: detail_path(conn, :index))
  end
end
