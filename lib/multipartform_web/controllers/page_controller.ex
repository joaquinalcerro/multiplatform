defmodule MultipartformWeb.PageController do
  use MultipartformWeb, :controller
  use Drab.Controller

  def index(conn, _params) do
    render(conn, "index.html", welcome_text: "Welcome to Phoenix")
  end
end
