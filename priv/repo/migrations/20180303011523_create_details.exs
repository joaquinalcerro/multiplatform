defmodule Multipartform.Repo.Migrations.CreateDetails do
  use Ecto.Migration

  def change do
    create table(:details) do
      add :tax, :boolean, default: false, null: false
      add :code, :integer
      add :desc, :string
      add :up, :decimal
      add :invoice_id, references(:invoices, on_delete: :nothing)

      timestamps()
    end

    create index(:details, [:invoice_id])
  end
end
