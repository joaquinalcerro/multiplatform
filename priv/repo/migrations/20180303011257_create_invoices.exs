defmodule Multipartform.Repo.Migrations.CreateInvoices do
  use Ecto.Migration

  def change do
    create table(:invoices) do
      add :provider, :string

      timestamps()
    end

  end
end
