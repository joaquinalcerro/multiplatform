defmodule Multipartform.DocumentsTest do
  use Multipartform.DataCase

  alias Multipartform.Documents

  describe "invoices" do
    alias Multipartform.Documents.Invoice

    @valid_attrs %{provider: "some provider"}
    @update_attrs %{provider: "some updated provider"}
    @invalid_attrs %{provider: nil}

    def invoice_fixture(attrs \\ %{}) do
      {:ok, invoice} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Documents.create_invoice()

      invoice
    end

    test "list_invoices/0 returns all invoices" do
      invoice = invoice_fixture()
      assert Documents.list_invoices() == [invoice]
    end

    test "get_invoice!/1 returns the invoice with given id" do
      invoice = invoice_fixture()
      assert Documents.get_invoice!(invoice.id) == invoice
    end

    test "create_invoice/1 with valid data creates a invoice" do
      assert {:ok, %Invoice{} = invoice} = Documents.create_invoice(@valid_attrs)
      assert invoice.provider == "some provider"
    end

    test "create_invoice/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Documents.create_invoice(@invalid_attrs)
    end

    test "update_invoice/2 with valid data updates the invoice" do
      invoice = invoice_fixture()
      assert {:ok, invoice} = Documents.update_invoice(invoice, @update_attrs)
      assert %Invoice{} = invoice
      assert invoice.provider == "some updated provider"
    end

    test "update_invoice/2 with invalid data returns error changeset" do
      invoice = invoice_fixture()
      assert {:error, %Ecto.Changeset{}} = Documents.update_invoice(invoice, @invalid_attrs)
      assert invoice == Documents.get_invoice!(invoice.id)
    end

    test "delete_invoice/1 deletes the invoice" do
      invoice = invoice_fixture()
      assert {:ok, %Invoice{}} = Documents.delete_invoice(invoice)
      assert_raise Ecto.NoResultsError, fn -> Documents.get_invoice!(invoice.id) end
    end

    test "change_invoice/1 returns a invoice changeset" do
      invoice = invoice_fixture()
      assert %Ecto.Changeset{} = Documents.change_invoice(invoice)
    end
  end

  describe "details" do
    alias Multipartform.Documents.Detail

    @valid_attrs %{code: 42, desc: "some desc", tax: true, up: "120.5"}
    @update_attrs %{code: 43, desc: "some updated desc", tax: false, up: "456.7"}
    @invalid_attrs %{code: nil, desc: nil, tax: nil, up: nil}

    def detail_fixture(attrs \\ %{}) do
      {:ok, detail} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Documents.create_detail()

      detail
    end

    test "list_details/0 returns all details" do
      detail = detail_fixture()
      assert Documents.list_details() == [detail]
    end

    test "get_detail!/1 returns the detail with given id" do
      detail = detail_fixture()
      assert Documents.get_detail!(detail.id) == detail
    end

    test "create_detail/1 with valid data creates a detail" do
      assert {:ok, %Detail{} = detail} = Documents.create_detail(@valid_attrs)
      assert detail.code == 42
      assert detail.desc == "some desc"
      assert detail.tax == true
      assert detail.up == Decimal.new("120.5")
    end

    test "create_detail/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Documents.create_detail(@invalid_attrs)
    end

    test "update_detail/2 with valid data updates the detail" do
      detail = detail_fixture()
      assert {:ok, detail} = Documents.update_detail(detail, @update_attrs)
      assert %Detail{} = detail
      assert detail.code == 43
      assert detail.desc == "some updated desc"
      assert detail.tax == false
      assert detail.up == Decimal.new("456.7")
    end

    test "update_detail/2 with invalid data returns error changeset" do
      detail = detail_fixture()
      assert {:error, %Ecto.Changeset{}} = Documents.update_detail(detail, @invalid_attrs)
      assert detail == Documents.get_detail!(detail.id)
    end

    test "delete_detail/1 deletes the detail" do
      detail = detail_fixture()
      assert {:ok, %Detail{}} = Documents.delete_detail(detail)
      assert_raise Ecto.NoResultsError, fn -> Documents.get_detail!(detail.id) end
    end

    test "change_detail/1 returns a detail changeset" do
      detail = detail_fixture()
      assert %Ecto.Changeset{} = Documents.change_detail(detail)
    end
  end
end
